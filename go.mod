module gitlab.com/gotils/coredb

go 1.15

require (
	github.com/jmoiron/sqlx v1.2.0
	github.com/pkg/errors v0.9.1
	google.golang.org/appengine v1.6.7 // indirect
)
